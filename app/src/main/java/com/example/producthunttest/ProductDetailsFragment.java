package com.example.producthunttest;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.producthunttest.models.Product;
import com.example.producthunttest.presenters.ProductDetailsPresenter;
import com.example.producthunttest.views.ProductDetailsView;
import com.squareup.picasso.Picasso;


public class ProductDetailsFragment extends Fragment implements ProductDetailsView {
    private ProductDetailsPresenter presenter;
    private View mview;
    private Product mProduct;

    public ProductDetailsFragment() {
        presenter = new ProductDetailsPresenter(this);
    }

    public static ProductDetailsFragment getInstance(Product product) {
        ProductDetailsFragment inst = new ProductDetailsFragment();
        inst.mProduct = product;
        inst.presenter.SetProduct(product);
        return inst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_product_details, container, false);
        Button btn = mview.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.GetIt();
            }
        });
        presenter.OnCreate();
        return mview;

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public ProductDetailsPresenter GetPresenter() {
        return presenter;
    }

    @Override
    public void ShowProduct(final Product product) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //insert values from Product instance to template
                ((TextView)mview.findViewById(R.id.Title)).setText(product.GetName());
                Picasso.with(getActivity().getApplicationContext())
                        .load(product.GetScreenshotUrl(mview.getWidth()))
                        .into(((ImageView) mview.findViewById(R.id.screenshot)));
                ((TextView)mview.findViewById(R.id.description)).setText(product.GetDescription());
                ((TextView)mview.findViewById(R.id.votes))
                        .setText(String.format("▲%s", String.valueOf(product.GetUpvotes())));
            }
        });
    }

    @Override
    public void StartIntent(Intent intent) {
        startActivity(intent);
    }


}
