package com.example.producthunttest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.producthunttest.models.Product;
import com.example.producthunttest.presenters.MainPresenter;
import com.example.producthunttest.presenters.ProductDetailsPresenter;
import com.example.producthunttest.views.MainView;

public class ProductHuntActivity extends AppCompatActivity implements MainView {
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_hunt);
        presenter = new MainPresenter(this);
        ProductListFragment fragment = new ProductListFragment();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment)
                .commit();
    }

    @Override
    public MainPresenter GetPresenter() {
        return presenter;
    }

    @Override
    public void ShowProductDetails(Product product) {
        ProductDetailsFragment fragment = ProductDetailsFragment.getInstance(product);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }
}
