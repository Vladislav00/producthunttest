package com.example.producthunttest;

import com.example.producthunttest.models.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import org.json.*;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ВЛАД on 19.08.2017.
 */

public class ProductHuntApi {
    private static final String host = "https://api.producthunt.com";
    private static final String token = "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff";

    public static void GetCategoryListAsync(final CategoryCallback callback) {
        final String endpoint = "/v1/categories";

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                OkHttpClient client = new OkHttpClient();
                Request.Builder builder = new Request.Builder();
                Request request = builder.url(host + endpoint)
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "Bearer " + token).build();

                try {
                    Response response = client.newCall(request).execute();
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray cats = jsonObject.getJSONArray("categories");
                    String[] r = new String[cats.length()];
                    for (int i = 0; i < cats.length(); i++) {
                        JSONObject obj = cats.getJSONObject(i);
                        r[i] = obj.getString("slug");
                    }
                    callback.CategoriesGetted(r);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    callback.CategoriesGetted(null);
                }
            }
        });
        t.start();
    }

    public interface CategoryCallback {
        void CategoriesGetted(String[] categories);
    }

    private static void _GetProductListAsync(final String category, final String params,
                                             final ProductListCallback callback) {
        final String endpoint = "/v1/posts/all";

        new Thread(new Runnable() {
            @Override
            public void run() {

                OkHttpClient client = new OkHttpClient();
                Request.Builder builder = new Request.Builder();
                Request request = builder.url(host + endpoint + String.format(Locale.ENGLISH, "?search[category]=%s%s", category, params))
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "Bearer " + token).build();

                try {
                    Response response = client.newCall(request).execute();
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    JSONArray prods = jsonObject.getJSONArray("posts");
                    ArrayList<Product> r = new ArrayList<Product>(prods.length());

                    for (int i = 0; i < prods.length(); i++) {
                        JSONObject obj = prods.getJSONObject(i);
                        r.add(new Product(obj));
                    }
                    callback.ProductsGetted(r);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    callback.ProductsGetted(null);
                }
            }
        }).start();
    }

    public static void GetProductListAsyncNewer(String category, int id, ProductListCallback callback) {
        _GetProductListAsync(category, String.format(Locale.ENGLISH, "&newer=%d", id), callback);
    }

    public static void GetProductListAsyncOlder(String category, int id, ProductListCallback callback) {
        _GetProductListAsync(category, String.format(Locale.ENGLISH, "&older=%d", id), callback);
    }

    public static void GetProductListAsync(String category, ProductListCallback callback) {
        _GetProductListAsync(category, "", callback);
    }

    public interface ProductListCallback {
        void ProductsGetted(ArrayList<Product> products);
    }


}
