package com.example.producthunttest;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.producthunttest.models.Product;
import com.example.producthunttest.presenters.ProductListPresenter;
import com.example.producthunttest.views.MainView;
import com.example.producthunttest.views.ProductListView;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class ProductListFragment extends Fragment implements ProductListView, ProductRecyclerViewAdapter.OnListInteractionListener {

    private ProductListPresenter mPresenter;
    private ProductRecyclerViewAdapter mAdapter;
    private View mView;
    private SwipeRefreshLayout swipeRefreshLayout;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ProductListFragment() {
        mPresenter = new ProductListPresenter(this);
    }

    public static ProductListFragment newInstance() {
        ProductListFragment fragment = new ProductListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_product_list, container, false);
        RecyclerView recyclerView = mView.findViewById(R.id.list);

        // Set the adapter
        mAdapter = new ProductRecyclerViewAdapter(new ArrayList<Product>(), this, this.getActivity().getApplicationContext());

        recyclerView.setAdapter(mAdapter);

        Spinner spinner = mView.findViewById(R.id.navigation_toolbar);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String category = (String) adapterView.getItemAtPosition(i);
                mPresenter.OnCategorySelected(category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        swipeRefreshLayout = mView.findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.OnReloadPull();
            }
        });

        mPresenter.OnCreate();
        return mView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainView) {
            mPresenter.AttachMainPresenter(((MainView) context).GetPresenter());
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement MainView");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPresenter.DetachMainPresenter();
    }

    @Override
    public void SetLoadingBar(final boolean visible) {
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(visible);
            }
        });
    }

    @Override
    public void SetItems(final List<Product> items) {
        final Context context = this.getActivity().getApplicationContext();
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(items!=null)
                    mAdapter.SetItemset(items);
            }
        });

    }

    @Override
    public void SetCategories(final String[] items) {
        final Context context = this.getActivity().getApplicationContext();
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Spinner spinner = mView.findViewById(R.id.navigation_toolbar);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_item, items);
                spinner.setAdapter(adapter);
            }
        });

    }

    @Override
    public void ShowText(String s) {
        Toast.makeText(getActivity().getApplicationContext(), s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void ScrollToTop() {
        final Context context = this.getActivity().getApplicationContext();
        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RecyclerView recyclerView = mView.findViewById(R.id.list);
                recyclerView.scrollToPosition(0);
            }
        });
    }

    @Override
    public void onListItemInteraction(Product item) {
        mPresenter.OnItemSelect(item);
    }
}
