package com.example.producthunttest;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.producthunttest.models.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Product} and makes a call to the
 * specified {@link OnListInteractionListener}.
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolder> {

    private List<Product> mValues;
    private final OnListInteractionListener mListener;
    private Context context;

    public ProductRecyclerViewAdapter(List<Product> items, OnListInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_product, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.SetItem(mValues.get(position));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListItemInteraction(holder.mItem);
                }
            }
        });
    }

    public void SetItemset(List<Product> items){
        mValues = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final View mView;
        private final ImageView mThumbnailView;
        private final TextView mNameView;
        private final TextView mDescriptionView;
        private final TextView mUpvotesView;
        private Product mItem;
        private final Context mcontext;

        ViewHolder(View view, Context context) {
            super(view);
            mView = view;
            mcontext = context;
            mThumbnailView = view.findViewById(R.id.product_thumbnail);
            mNameView = view.findViewById(R.id.product_title);
            mDescriptionView = view.findViewById(R.id.product_description);
            mUpvotesView = view.findViewById(R.id.product_upvotes);
        }

        void SetItem(final Product item){
            mItem = item;
            Picasso.with(mcontext).load(item.GetThumbnailUrl()).into(mThumbnailView);
            mNameView.setText(item.GetName());
            mDescriptionView.setText(item.GetDescription());
            mUpvotesView.setText(String.format("▲%s", String.valueOf(item.GetUpvotes())));
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }

    public interface OnListInteractionListener{
        void onListItemInteraction(Product item);
    }
}
