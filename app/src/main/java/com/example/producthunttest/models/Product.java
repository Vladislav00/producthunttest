package com.example.producthunttest.models;

import com.example.producthunttest.ProductHuntApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

/**
 * Created by ВЛАД on 21.08.2017.
 */

public class Product {
    private int id;
    private String title; //name
    private String thumbnail_url;
    private String description; //tagline
    private HashMap<Integer, String> screenshot_url;
    private String redirect_url;
    private int votes_count;

    public Product(int id, String title, String thumbnail_url, String description,
                   HashMap<Integer, String> screenshot_url, String redirect_url, int votes_count) {
        this.id = id;
        this.title = title; //name
        this.thumbnail_url = thumbnail_url;
        this.description = description;
        this.screenshot_url = screenshot_url;
        this.redirect_url = redirect_url;
        this.votes_count = votes_count;
    }

    public Product(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getInt("id");
        title = jsonObject.getString("name");
        thumbnail_url = jsonObject.getJSONObject("thumbnail").getString("image_url");
        description = jsonObject.getString("tagline");
        redirect_url = jsonObject.getString("redirect_url");
        votes_count = jsonObject.getInt("votes_count");
        HashMap<Integer, String> hm = new HashMap<>();
        JSONObject screenshots = jsonObject.getJSONObject("screenshot_url");

        for (Iterator<String> iter = screenshots.keys(); iter.hasNext(); ) {
            String k = iter.next();
            String v = screenshots.getString(k);
            hm.put(Integer.valueOf(k.substring(0, k.length() - 2)), v);
        }
        screenshot_url = hm;
    }

    public String GetName() {
        return title;
    }

    public String GetDescription() {
        return description;
    }

    public String GetRedirectUrl() {
        return redirect_url;
    }

    public String GetThumbnailUrl() {
        return thumbnail_url;
    }

    public String GetScreenshotUrl(int size) {
        int mk = 0, md = Integer.MAX_VALUE;
        for (int k : screenshot_url.keySet()) {
            int d = Math.abs(k - size);
            if (d < md) {
                md = d;
                mk = k;
            }
        }
        return screenshot_url.get(mk);
    }

    public int GetUpvotes() {
        return votes_count;
    }

    public static void GetProductListAsyncNewer(String category, Product t, ProductHuntApi.ProductListCallback callback) {
        ProductHuntApi.GetProductListAsyncNewer(category, t.id, callback);
    }

    public static void GetProductListAsyncOlder(String category, Product t, ProductHuntApi.ProductListCallback callback) {
        ProductHuntApi.GetProductListAsyncOlder(category, t.id, callback);
    }

    public static void GetProductListAsync(String category, ProductHuntApi.ProductListCallback callback) {
        ProductHuntApi.GetProductListAsync(category, callback);
    }

}
