package com.example.producthunttest.presenters;

import com.example.producthunttest.models.Product;
import com.example.producthunttest.views.MainView;

/**
 * Created by ВЛАД on 21.08.2017.
 */

public class MainPresenter {
    private MainView view;

    public MainPresenter(MainView mainView) {
        view = mainView;
    }

    public void onProductSelected(Product product) {
        view.ShowProductDetails(product);

    }
}
