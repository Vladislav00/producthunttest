package com.example.producthunttest.presenters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.example.producthunttest.models.Product;
import com.example.producthunttest.views.ProductDetailsView;

/**
 * Created by ВЛАД on 22.08.2017.
 */

public class ProductDetailsPresenter {
    private ProductDetailsView view;
    private Product mProduct;


    public ProductDetailsPresenter(ProductDetailsView view){
        this.view = view;
    }

    public void SetProduct(Product product){
        mProduct = product;
    }

    public void GetIt(){
        //start intent - open web page
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mProduct.GetRedirectUrl()));
        view.StartIntent(browserIntent);
    }

    public void OnCreate() {
        view.ShowProduct(mProduct);
    }
}
