package com.example.producthunttest.presenters;

import android.widget.Toast;

import com.example.producthunttest.ProductHuntApi;
import com.example.producthunttest.models.Product;
import com.example.producthunttest.views.ProductListView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * Created by ВЛАД on 21.08.2017.
 */

public class ProductListPresenter {
    private ProductListView view;
    private MainPresenter mainPresenter;

    private String current_category;
    private List<Product> current_products;

    public ProductListPresenter(ProductListView view){
        this.view = view;
    }

    public void AttachMainPresenter(MainPresenter presenter){
        mainPresenter = presenter;
    }

    public void DetachMainPresenter(){
        mainPresenter = null;
    }

    public void OnCategorySelected(String category){
        if (!Objects.equals(category, current_category)){
            view.SetLoadingBar(true);
            current_category=category;
            Product.GetProductListAsync(category, new ProductHuntApi.ProductListCallback() {
                @Override
                public void ProductsGetted(ArrayList<Product> products) {
                    current_products = products;
                    view.SetItems(current_products);
                    view.SetLoadingBar(false);
                    view.ScrollToTop();
                }
            });

        }

    }
    public void OnReloadPull(){
        if (current_products!=null && current_products.size()>0){
             Product.GetProductListAsyncNewer(current_category, current_products.get(0), new ProductHuntApi.ProductListCallback() {
                @Override
                public void ProductsGetted(ArrayList<Product> products) {
                    products.addAll(current_products);
                    current_products = products;
                    view.SetItems(products);
                    view.SetLoadingBar(false);
                }
            });
        }
    }
    public void OnBottomScroll(){
        //get products older than last prod.
    }
    public void OnItemSelect(Product item){
        if (mainPresenter!=null){
            mainPresenter.onProductSelected(item);
        }
    }

    public void OnCreate(){
        //get categories async
        view.SetLoadingBar(true);
        ProductHuntApi.GetCategoryListAsync(new ProductHuntApi.CategoryCallback() {
            @Override
            public void CategoriesGetted(String[] categories) {
                if (categories!=null)
                    view.SetCategories(categories);
                else
                    view.ShowText("Some error");
                view.SetLoadingBar(false);
            }
        });
    }
}
