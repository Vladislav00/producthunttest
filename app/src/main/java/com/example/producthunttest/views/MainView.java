package com.example.producthunttest.views;

import com.example.producthunttest.models.Product;
import com.example.producthunttest.presenters.MainPresenter;

/**
 * Created by ВЛАД on 21.08.2017.
 */

public interface MainView {
    MainPresenter GetPresenter();

    void ShowProductDetails(Product product);
}
