package com.example.producthunttest.views;

import android.content.Intent;

import com.example.producthunttest.models.Product;
import com.example.producthunttest.presenters.ProductDetailsPresenter;

/**
 * Created by ВЛАД on 22.08.2017.
 */

public interface ProductDetailsView {
    ProductDetailsPresenter GetPresenter();
    void ShowProduct(Product product);
    void StartIntent(Intent intent);
}
