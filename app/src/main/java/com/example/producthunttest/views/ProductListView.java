package com.example.producthunttest.views;

import com.example.producthunttest.models.Product;

import java.util.List;

/**
 * Created by ВЛАД on 21.08.2017.
 */

public interface ProductListView {
    void SetLoadingBar(boolean visible);
    void SetItems(List<Product> items);
    void SetCategories(String[] items);
    void ShowText(String s);
    void ScrollToTop();
}
